(defparameter *grid-easy*
  #2A((0 4 0 0 0 0 1 7 9)
      (0 0 2 0 0 8 0 5 4)
      (0 0 6 0 0 5 0 0 8)
      (0 8 0 0 7 0 9 1 0)
      (0 5 0 0 9 0 0 3 0)
      (0 1 9 0 6 0 0 4 0)
      (3 0 0 4 0 0 7 0 0)
      (5 7 0 1 0 0 2 0 0)
      (9 2 8 0 0 0 0 6 0)))

(defparameter *grid-hard*
  #2A((0 0 0 7 0 0 8 0 0)
      (0 0 6 0 0 0 0 3 1)
      (0 4 0 0 0 2 0 0 0)
      (0 2 4 0 7 0 0 0 0)
      (0 1 0 0 3 0 0 8 0)
      (0 0 0 0 6 0 2 9 0)
      (0 0 0 8 0 0 0 7 0)
      (8 6 0 0 0 0 5 0 0)
      (0 0 2 0 0 6 0 0 0)))

(defun copy-array (array &key
                           (element-type (array-element-type array))
                           (fill-pointer (and (array-has-fill-pointer-p array)
                                              (fill-pointer array)))
                           (adjustable (adjustable-array-p array)))
  "Returns an undisplaced copy of ARRAY, with same fill-pointer and
adjustability (if any) as the original, unless overridden by the keyword
arguments."
  (let* ((dimensions (array-dimensions array))
         (new-array (make-array dimensions
                                :element-type element-type
                                :adjustable adjustable
                                :fill-pointer fill-pointer)))
    (dotimes (i (array-total-size array))
      (setf (row-major-aref new-array i)
            (row-major-aref array i)))
    new-array))

(defun remove-zero (seq)
  (remove-if #'zerop seq))

(defun remove-nil (seq)
  (remove-if #'null seq))

(defun blank-grid ()
  (make-array '(9 9) :initial-element 0))

(defun set-cell (grid coord val)
  (destructuring-bind (row col) coord
    (setf (aref grid row col) val)))

(defun row (grid row)
  (loop :for x :from 0 :to 8
        :collecting (aref grid row x)))

(defun col (grid col)
  (loop :for x :from 0 :to 8
        :collecting (aref grid x col)))

(defun cluster-idx (row col)
  (let ((cell-row (if (zerop row) 0 (floor row 3)))
        (cell-col (if (zerop col) 0 (floor col 3))))
    (list cell-row cell-col)))

(defun cluster (grid row col)
  (destructuring-bind (cell-row cell-col) (cluster-idx row col)
    (loop :for dr :from 0 :to 2
          :append (loop :for dc :from 0 :to 2
                        :for r = (+ (* 3 cell-row) dr)
                        :for c = (+ (* 3 cell-col) dc)
                        :collecting (aref grid r c)))))

(defun options (grid row col)
  (when (zerop (aref grid row col))
    (let ((constraints (remove-zero (nconc (row grid row)
                                           (col grid col)
                                           (cluster grid row col)))))
      (set-difference '(1 2 3 4 5 6 7 8 9) constraints))))

(defun empty-cells (grid)
  (loop :for row :from 0 :to 8
        :append (loop :for col :from 0 :to 8
                      :when (zerop (aref grid row col))
                        :collect (list row col))))

(defun solvedp (grid)
  (zerop (list-length (empty-cells grid))))

(defun solve (grid)
  (if (solvedp grid)
      grid
      (labels ((map-options (coord)
                 (destructuring-bind (row col) coord
                   (options grid row col)))
               (zip (seq1 seq2)
                 (mapcar #'list seq1 seq2))
               (pair-lengthp (pair1 pair2)
                 (< (list-length (second pair1)) (list-length (second pair2)))))
        (let* ((empties (empty-cells grid))
               (options (mapcar #'map-options empties))
               (option-pairs (sort (zip empties options) #'pair-lengthp)))
          (loop :for pair :in option-pairs
                :for coord = (first pair)
                :for options = (second pair)
                :do (loop :for option :in options
                          :for new-grid = (let ((new-grid (copy-array grid)))
                                            (set-cell new-grid coord option)
                                            (solve new-grid))
                          :when (not (null new-grid))
                            :do (return-from solve new-grid)))))))
